-- remove `
-- remove 'unsigned'
-- int NOT NULL AUTO_INCREMENT -> serial
-- double -> float
-- is_show\is_read\is_... -> bool
-- //ulService.save(new LoginRecord(ip, new Date(), info, user));

-- ----------------------------
-- Table structure for aoa_attachment_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_attachment_list;
CREATE TABLE aoa_attachment_list (
  attachment_id bigint NOT NULL,
  attachment_name varchar(255) DEFAULT NULL,
  attachment_path varchar(255) DEFAULT NULL,
  attachment_shuffix varchar(255) DEFAULT NULL,
  attachment_size varchar(255) DEFAULT NULL,
  attachment_type varchar(255) DEFAULT NULL,
  model varchar(255) DEFAULT NULL,
  upload_time date DEFAULT NULL,
  user_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (attachment_id)
);


-- ----------------------------
-- Table structure for aoa_attends_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_attends_list;
CREATE TABLE aoa_attends_list (
  attends_id serial,
  attends_ip varchar(20) DEFAULT NULL,
  attends_remark varchar(20) DEFAULT NULL,
  attends_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  type_id int DEFAULT NULL,
  attends_user_id int DEFAULT NULL,
  attend_hmtime varchar(255) DEFAULT NULL,
  week_ofday varchar(255) DEFAULT NULL,
  holiday_days float DEFAULT '0',
  holiday_start date DEFAULT NULL,
  PRIMARY KEY (attends_id)
);

-- ----------------------------
-- Table structure for aoa_bursement
-- ----------------------------
DROP TABLE IF EXISTS aoa_bursement;
CREATE TABLE aoa_bursement (
  bursement_id serial,
  all_money float DEFAULT NULL,
  allinvoices int DEFAULT NULL,
  burse_time date DEFAULT NULL,
  financial_advice varchar(255) DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  name varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  operation_name int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  user_name int DEFAULT NULL,
  PRIMARY KEY (bursement_id)
);

-- ----------------------------
-- Table structure for aoa_catalog
-- ----------------------------
DROP TABLE IF EXISTS aoa_catalog;
CREATE TABLE aoa_catalog (
  catalog_id serial,
  catalog_name varchar(255) DEFAULT NULL,
  parent_id int DEFAULT NULL,
  cata_user_id int DEFAULT NULL,
  PRIMARY KEY (catalog_id)
);


-- ----------------------------
-- Table structure for aoa_comment_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_comment_list;
CREATE TABLE aoa_comment_list (
  comment_id serial,
  comment varchar(255) DEFAULT NULL,
  time date DEFAULT NULL,
  comment_user_id int DEFAULT NULL,
  reply_id int DEFAULT NULL,
  PRIMARY KEY (comment_id)
);

-- ----------------------------
-- Table structure for aoa_dept
-- ----------------------------
DROP TABLE IF EXISTS aoa_dept;
CREATE TABLE aoa_dept (
  dept_id serial,
  dept_addr varchar(255) DEFAULT NULL,
  dept_fax varchar(255) DEFAULT NULL,
  dept_name varchar(255) DEFAULT NULL,
  dept_tel varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  deptmanager int DEFAULT NULL,
  end_time date DEFAULT NULL,
  start_time date DEFAULT NULL,
  PRIMARY KEY (dept_id)
);


-- ----------------------------
-- Table structure for aoa_detailsburse
-- ----------------------------
DROP TABLE IF EXISTS aoa_detailsburse;
CREATE TABLE aoa_detailsburse (
  detailsburse_id serial,
  descript varchar(255) DEFAULT NULL,
  detailmoney float NOT NULL,
  invoices int DEFAULT NULL,
  produce_time date DEFAULT NULL,
  subject varchar(255) DEFAULT NULL,
  bursment_id int DEFAULT NULL,
  PRIMARY KEY (detailsburse_id)
);

-- ----------------------------
-- Table structure for aoa_director
-- ----------------------------
DROP TABLE IF EXISTS aoa_director;
CREATE TABLE aoa_director (
  director_id serial,
  address varchar(255) DEFAULT NULL,
  company_number varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  image_path int DEFAULT NULL,
  phone_number varchar(255) DEFAULT NULL,
  pinyin varchar(255) DEFAULT NULL,
  remark varchar(255) DEFAULT NULL,
  sex varchar(255) DEFAULT NULL,
  user_name varchar(255) DEFAULT NULL,
  user_id int DEFAULT NULL,
  companyname varchar(255) DEFAULT NULL,
  PRIMARY KEY (director_id)
);

-- ----------------------------
-- Table structure for aoa_director_users
-- ----------------------------
DROP TABLE IF EXISTS aoa_director_users;
CREATE TABLE aoa_director_users (
  director_users_id serial,
  catelog_name varchar(255) DEFAULT NULL,
  is_handle bool DEFAULT NULL,
  director_id int DEFAULT NULL,
  user_id int DEFAULT NULL,
  share_user_id int DEFAULT NULL,
  sharetime date DEFAULT NULL,
  PRIMARY KEY (director_users_id)
);

-- ----------------------------
-- Table structure for aoa_discuss_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_discuss_list;
CREATE TABLE aoa_discuss_list (
  discuss_id serial,
  attachment_id int DEFAULT NULL,
  content text,
  create_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  visit_num int DEFAULT NULL,
  discuss_user_id int DEFAULT NULL,
  vote_id int DEFAULT NULL,
  modify_time date DEFAULT NULL,
  PRIMARY KEY (discuss_id)
);

-- ----------------------------
-- Table structure for aoa_evection
-- ----------------------------
DROP TABLE IF EXISTS aoa_evection;
CREATE TABLE aoa_evection (
  evection_id serial,
  type_id int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  personnel_advice varchar(255) DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  PRIMARY KEY (evection_id)
);


-- ----------------------------
-- Table structure for aoa_evectionmoney
-- ----------------------------
DROP TABLE IF EXISTS aoa_evectionmoney;
CREATE TABLE aoa_evectionmoney (
  evectionmoney_id serial,
  financial_advice varchar(255) DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  money float NOT NULL,
  name varchar(255) DEFAULT NULL,
  pro_id int DEFAULT NULL,
  pro int DEFAULT NULL,
  PRIMARY KEY (evectionmoney_id)
);


-- ----------------------------
-- Table structure for aoa_file_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_file_list;
CREATE TABLE aoa_file_list (
  file_id serial,
  file_name varchar(255) DEFAULT NULL,
  file_path varchar(255) DEFAULT NULL,
  file_shuffix varchar(255) DEFAULT NULL,
  content_type varchar(255) DEFAULT NULL,
  model varchar(255) DEFAULT NULL,
  path_id int DEFAULT NULL,
  size int DEFAULT NULL,
  upload_time date DEFAULT NULL,
  file_user_id int DEFAULT NULL,
  file_istrash int DEFAULT '0',
  file_isshare int DEFAULT NULL,
  PRIMARY KEY (file_id)
);

-- ----------------------------
-- Table structure for aoa_file_path
-- ----------------------------
DROP TABLE IF EXISTS aoa_file_path;
CREATE TABLE aoa_file_path (
  path_id serial,
  parent_id int DEFAULT NULL,
  path_name varchar(255) DEFAULT NULL,
  path_user_id int DEFAULT NULL,
  path_istrash int DEFAULT '0',
  PRIMARY KEY (path_id)
);

-- ----------------------------
-- Table structure for aoa_holiday
-- ----------------------------
DROP TABLE IF EXISTS aoa_holiday;
CREATE TABLE aoa_holiday (
  holiday_id serial,
  leave_days int DEFAULT NULL,
  type_id int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  personnel_advice varchar(255) DEFAULT NULL,
  PRIMARY KEY (holiday_id)
);


-- ----------------------------
-- Table structure for aoa_in_mail_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_in_mail_list;
CREATE TABLE aoa_in_mail_list (
  mail_id serial,
  mail_content text,
  mail_create_time date DEFAULT NULL,
  mail_file_id int DEFAULT NULL,
  mail_title varchar(255) NOT NULL,
  mail_type int DEFAULT NULL,
  mail_in_push_user_id int DEFAULT NULL,
  in_receiver varchar(255) DEFAULT NULL,
  mail_status_id int DEFAULT NULL,
  mail_number_id int DEFAULT NULL,
  mail_del int DEFAULT NULL,
  mail_push int DEFAULT NULL,
  mail_star int DEFAULT NULL,
  PRIMARY KEY (mail_id)
);


-- ----------------------------
-- Table structure for aoa_love_discuss_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_love_discuss_user;
CREATE TABLE aoa_love_discuss_user (
  discuss_id int NOT NULL,
  user_id int NOT NULL,
  PRIMARY KEY (discuss_id,user_id)
);


-- ----------------------------
-- Table structure for aoa_love_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_love_user;
CREATE TABLE aoa_love_user (
  reply_id int NOT NULL,
  user_id int NOT NULL
);


-- ----------------------------
-- Table structure for aoa_mail_reciver
-- ----------------------------
DROP TABLE IF EXISTS aoa_mail_reciver;
CREATE TABLE aoa_mail_reciver (
  pk_id serial,
  is_read bool NOT NULL,
  mail_id int DEFAULT NULL,
  mail_reciver_id int DEFAULT NULL,
  is_star bool DEFAULT NULL,
  is_del bool DEFAULT NULL,
  PRIMARY KEY (pk_id)
);

-- ----------------------------
-- Table structure for aoa_mailnumber
-- ----------------------------
DROP TABLE IF EXISTS aoa_mailnumber;
CREATE TABLE aoa_mailnumber (
  mail_number_id serial,
  mail_account varchar(255) NOT NULL,
  mail_create_time date DEFAULT NULL,
  mail_des varchar(255) DEFAULT NULL,
  mail_type int DEFAULT NULL,
  mail_user_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  status int DEFAULT NULL,
  mail_num_user_id int DEFAULT NULL,
  PRIMARY KEY (mail_number_id)
);


-- ----------------------------
-- Table structure for aoa_note_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_note_list;
CREATE TABLE aoa_note_list (
  note_id serial,
  content varchar(8000) DEFAULT NULL,
  create_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  catalog_id int DEFAULT NULL,
  attach_id int DEFAULT NULL,
  is_collected int DEFAULT 0,
  createman_id int DEFAULT NULL,
  receiver varchar(255) DEFAULT NULL,
  PRIMARY KEY (note_id)
);

-- ----------------------------
-- Table structure for aoa_notepaper
-- ----------------------------
DROP TABLE IF EXISTS aoa_notepaper;
CREATE TABLE aoa_notepaper (
  notepaper_id serial,
  concent text,
  create_time date DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  notepaper_user_id int DEFAULT NULL,
  PRIMARY KEY (notepaper_id)
);


-- ----------------------------
-- Table structure for aoa_notice_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_notice_list;
CREATE TABLE aoa_notice_list (
  notice_id serial,
  content varchar(255) DEFAULT NULL,
  is_top bool DEFAULT NULL,
  modify_time date DEFAULT NULL,
  notice_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  url varchar(255) DEFAULT NULL,
  user_id int DEFAULT NULL,
  PRIMARY KEY (notice_id)
);

-- ----------------------------
-- Table structure for aoa_notice_user_relation
-- ----------------------------
DROP TABLE IF EXISTS aoa_notice_user_relation;
CREATE TABLE aoa_notice_user_relation (
  relatin_id serial,
  is_read bool DEFAULT NULL,
  relatin_notice_id int DEFAULT NULL,
  relatin_user_id int DEFAULT NULL,
  PRIMARY KEY (relatin_id)
);


-- ----------------------------
-- Table structure for aoa_overtime
-- ----------------------------
DROP TABLE IF EXISTS aoa_overtime;
CREATE TABLE aoa_overtime (
  overtime_id serial,
  type_id int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  personnel_advice varchar(255) DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  PRIMARY KEY (overtime_id)
);

-- ----------------------------
-- Table structure for aoa_plan_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_plan_list;
CREATE TABLE aoa_plan_list (
  plan_id serial,
  create_time date DEFAULT NULL,
  end_time date DEFAULT NULL,
  label varchar(255) DEFAULT NULL,
  plan_comment varchar(5000) DEFAULT '',
  plan_content varchar(8000) DEFAULT NULL,
  plan_summary varchar(800) DEFAULT NULL,
  start_time date NOT NULL,
  status_id int DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  plan_user_id int DEFAULT NULL,
  attach_id int DEFAULT NULL,
  PRIMARY KEY (plan_id,start_time)
);

-- ----------------------------
-- Table structure for aoa_position
-- ----------------------------
DROP TABLE IF EXISTS aoa_position;
CREATE TABLE aoa_position (
  position_id serial,
  level int DEFAULT NULL,
  name varchar(255) DEFAULT NULL,
  describtion varchar(255) DEFAULT NULL,
  deptid int DEFAULT NULL,
  PRIMARY KEY (position_id)
);


-- ----------------------------
-- Table structure for aoa_process_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_process_list;
CREATE TABLE aoa_process_list (
  process_id serial,
  apply_time date DEFAULT NULL,
  deeply_id int DEFAULT NULL,
  end_time date DEFAULT NULL,
  process_des text,
  process_name varchar(255) DEFAULT NULL,
  procsee_days int DEFAULT NULL,
  is_checked bool DEFAULT NULL,
  start_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  type_name varchar(255) DEFAULT NULL,
  pro_file_id int DEFAULT NULL,
  process_user_id int DEFAULT NULL,
  shenuser varchar(255) DEFAULT NULL,
  PRIMARY KEY (process_id)
);


-- ----------------------------
-- Table structure for aoa_receiver_note
-- ----------------------------
DROP TABLE IF EXISTS aoa_receiver_note;
CREATE TABLE aoa_receiver_note (
  note_id int NOT NULL,
  user_id int NOT NULL,
  id serial,
  PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for aoa_regular
-- ----------------------------
DROP TABLE IF EXISTS aoa_regular;
CREATE TABLE aoa_regular (
  regular_id serial,
  advice varchar(255) DEFAULT NULL,
  deficiency varchar(255) DEFAULT NULL,
  dobetter varchar(255) DEFAULT NULL,
  experience varchar(255) DEFAULT NULL,
  personnel_advice varchar(255) DEFAULT NULL,
  pullulate varchar(255) DEFAULT NULL,
  understand varchar(255) DEFAULT NULL,
  pro_id int DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  days int DEFAULT NULL,
  PRIMARY KEY (regular_id)
);


-- ----------------------------
-- Table structure for aoa_reply_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_reply_list;
CREATE TABLE aoa_reply_list (
  reply_id serial,
  content text,
  replay_time date DEFAULT NULL,
  discuss_id int DEFAULT NULL,
  reply_user_id int DEFAULT NULL,
  PRIMARY KEY (reply_id)
);

-- ----------------------------
-- Table structure for aoa_resign
-- ----------------------------
DROP TABLE IF EXISTS aoa_resign;
CREATE TABLE aoa_resign (
  resign_id serial,
  financial_advice varchar(255) DEFAULT NULL,
  is_finish bool DEFAULT NULL,
  nofinish varchar(255) DEFAULT NULL,
  personnel_advice varchar(255) DEFAULT NULL,
  suggest varchar(255) DEFAULT NULL,
  hand_user int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  manager_advice varchar(255) DEFAULT NULL,
  PRIMARY KEY (resign_id)
);

-- ----------------------------
-- Table structure for aoa_reviewed
-- ----------------------------
DROP TABLE IF EXISTS aoa_reviewed;
CREATE TABLE aoa_reviewed (
  reviewed_id serial,
  advice varchar(255) DEFAULT NULL,
  reviewed_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  pro_id int DEFAULT NULL,
  user_id int DEFAULT NULL,
  del int DEFAULT NULL,
  PRIMARY KEY (reviewed_id)
);

-- ----------------------------
-- Table structure for aoa_role_
-- ----------------------------
DROP TABLE IF EXISTS aoa_role_;
CREATE TABLE aoa_role_ (
  role_id serial,
  role_name varchar(255) DEFAULT NULL,
  role_value int DEFAULT NULL,
  PRIMARY KEY (role_id)
);


-- ----------------------------
-- Table structure for aoa_role_power_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_role_power_list;
CREATE TABLE aoa_role_power_list (
  pk_id serial,
  is_show bool DEFAULT NULL,
  menu_id int DEFAULT NULL,
  role_id int DEFAULT NULL,
  PRIMARY KEY (pk_id)
);

-- ----------------------------
-- Table structure for aoa_schedule_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_schedule_list;
CREATE TABLE aoa_schedule_list (
  rc_id serial,
  create_time date DEFAULT NULL,
  end_time date DEFAULT NULL,
  filedescribe varchar(255) DEFAULT NULL,
  is_remind bool DEFAULT NULL,
  start_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type_id int DEFAULT NULL,
  user_id int DEFAULT NULL,
  miaoshu varchar(255) DEFAULT NULL,
  isreminded int DEFAULT NULL,
  PRIMARY KEY (rc_id)
);

-- ----------------------------
-- Table structure for aoa_schedule_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_schedule_user;
CREATE TABLE aoa_schedule_user (
  rcid int NOT NULL,
  user_id int NOT NULL
);

-- ----------------------------
-- Table structure for aoa_status_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_status_list;
CREATE TABLE aoa_status_list (
  status_id serial,
  status_color varchar(255) DEFAULT NULL,
  status_model varchar(255) DEFAULT NULL,
  status_name varchar(255) DEFAULT NULL,
  sort_value int DEFAULT NULL,
  sort_precent varchar(255) DEFAULT NULL,
  PRIMARY KEY (status_id)
);


-- ----------------------------
-- Table structure for aoa_stay
-- ----------------------------
DROP TABLE IF EXISTS aoa_stay;
CREATE TABLE aoa_stay (
  stay_id serial,
  day int DEFAULT NULL,
  hotel_name varchar(255) DEFAULT NULL,
  leave_time date DEFAULT NULL,
  stay_city varchar(255) DEFAULT NULL,
  stay_money float DEFAULT NULL,
  stay_time date DEFAULT NULL,
  evemoney_id int DEFAULT NULL,
  user_name int DEFAULT NULL,
  PRIMARY KEY (stay_id)
);


-- ----------------------------
-- Table structure for aoa_subject
-- ----------------------------
DROP TABLE IF EXISTS aoa_subject;
CREATE TABLE aoa_subject (
  subject_id serial,
  name varchar(255) DEFAULT NULL,
  parent_id int DEFAULT NULL,
  PRIMARY KEY (subject_id)
);

-- ----------------------------
-- Table structure for aoa_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS aoa_sys_menu;
CREATE TABLE aoa_sys_menu (
  menu_id serial,
  is_show bool DEFAULT NULL,
  menu_grade int DEFAULT NULL,
  menu_icon varchar(255) DEFAULT NULL,
  menu_name varchar(255) DEFAULT NULL,
  menu_url varchar(255) NOT NULL,
  parent_id int DEFAULT NULL,
  sort_id int DEFAULT NULL,
  PRIMARY KEY (menu_id)
);

-- ----------------------------
-- Table structure for aoa_task_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_task_list;
CREATE TABLE aoa_task_list (
  task_id serial,
  comment varchar(255) DEFAULT NULL,
  end_time date DEFAULT NULL,
  is_cancel bool DEFAULT '0',
  is_top bool DEFAULT '0',
  modify_time date DEFAULT NULL,
  publish_time date DEFAULT NULL,
  star_time date DEFAULT NULL,
  status_id int DEFAULT NULL,
  task_describe varchar(255) NOT NULL,
  ticking varchar(255) DEFAULT NULL,
  title varchar(255) NOT NULL,
  type_id int DEFAULT NULL,
  task_push_user_id int DEFAULT NULL,
  reciverlist varchar(255) DEFAULT NULL,
  PRIMARY KEY (task_id)
);

-- ----------------------------
-- Table structure for aoa_task_logger
-- ----------------------------
DROP TABLE IF EXISTS aoa_task_logger;
CREATE TABLE aoa_task_logger (
  logger_id serial,
  create_time date DEFAULT NULL,
  logger_ticking varchar(255) DEFAULT NULL,
  task_id int DEFAULT NULL,
  username varchar(255) DEFAULT NULL,
  logger_statusid int DEFAULT NULL,
  PRIMARY KEY (logger_id)
);

-- ----------------------------
-- Table structure for aoa_task_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_task_user;
CREATE TABLE aoa_task_user (
  pk_id serial,
  status_id int DEFAULT NULL,
  task_id int DEFAULT NULL,
  task_recive_user_id int DEFAULT NULL,
  PRIMARY KEY (pk_id)
);


-- ----------------------------
-- Table structure for aoa_traffic
-- ----------------------------
DROP TABLE IF EXISTS aoa_traffic;
CREATE TABLE aoa_traffic (
  traffic_id serial,
  depart_name varchar(255) DEFAULT NULL,
  depart_time date DEFAULT NULL,
  reach_name varchar(255) DEFAULT NULL,
  seat_type varchar(255) DEFAULT NULL,
  traffic_money float DEFAULT NULL,
  traffic_name varchar(255) DEFAULT NULL,
  evection_id int DEFAULT NULL,
  user_name int DEFAULT NULL,
  PRIMARY KEY (traffic_id)
);


-- ----------------------------
-- Table structure for aoa_type_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_type_list;
CREATE TABLE aoa_type_list (
  type_id serial,
  type_color varchar(255) DEFAULT NULL,
  type_model varchar(255) DEFAULT NULL,
  type_name varchar(255) DEFAULT NULL,
  sort_value int DEFAULT NULL,
  PRIMARY KEY (type_id)
);

-- ----------------------------
-- Table structure for aoa_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_user;
CREATE TABLE aoa_user (
  user_id serial,
  address varchar(255) DEFAULT NULL,
  bank varchar(255) DEFAULT NULL,
  birth date DEFAULT NULL,
  eamil varchar(255) DEFAULT NULL,
  father_id int DEFAULT NULL,
  hire_time date DEFAULT NULL,
  user_idcard varchar(255) DEFAULT NULL,
  img_path varchar(255) DEFAULT NULL,
  is_lock int DEFAULT NULL,
  last_login_ip varchar(255) DEFAULT NULL,
  last_login_time date DEFAULT NULL,
  modify_time date DEFAULT NULL,
  modify_user_id int DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  real_name varchar(255) DEFAULT NULL,
  salary float DEFAULT NULL,
  user_school varchar(255) DEFAULT NULL,
  sex varchar(255) DEFAULT NULL,
  theme_skin varchar(255) DEFAULT NULL,
  user_edu varchar(255) DEFAULT NULL,
  user_name varchar(255) DEFAULT NULL,
  user_sign varchar(255) DEFAULT NULL,
  user_tel varchar(255) DEFAULT NULL,
  dept_id int DEFAULT NULL,
  position_id int DEFAULT NULL,
  role_id int DEFAULT NULL,
  superman int DEFAULT NULL,
  holiday int DEFAULT NULL,
  pinyin varchar(255) DEFAULT NULL,
  PRIMARY KEY (user_id)
);

-- ----------------------------
-- Table structure for aoa_user_log
-- ----------------------------
DROP TABLE IF EXISTS aoa_user_log;
CREATE TABLE aoa_user_log (
  log_id serial,
  ip_addr varchar(255) DEFAULT NULL,
  log_time date DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  url varchar(255) DEFAULT NULL,
  user_id int DEFAULT NULL,
  PRIMARY KEY (log_id)
);

-- ----------------------------
-- Table structure for aoa_user_login_record
-- ----------------------------
DROP TABLE IF EXISTS aoa_user_login_record;
CREATE TABLE aoa_user_login_record (
  record_id serial,
  browser varchar(255) DEFAULT NULL,
  ip_addr varchar(255) DEFAULT NULL,
  login_time date DEFAULT NULL,
  session_id varchar(255) DEFAULT NULL,
  user_id int DEFAULT NULL,
  PRIMARY KEY (record_id)
);

-- ----------------------------
-- Table structure for aoa_vote_list
-- ----------------------------
DROP TABLE IF EXISTS aoa_vote_list;
CREATE TABLE aoa_vote_list (
  vote_id serial,
  end_time date DEFAULT NULL,
  selectone int DEFAULT NULL,
  start_time date DEFAULT NULL,
  PRIMARY KEY (vote_id)
);


-- ----------------------------
-- Table structure for aoa_vote_title_user
-- ----------------------------
DROP TABLE IF EXISTS aoa_vote_title_user;
CREATE TABLE aoa_vote_title_user (
  vote_title_user_id serial,
  vote_id int DEFAULT NULL,
  user_id int DEFAULT NULL,
  title_id int DEFAULT NULL,
  PRIMARY KEY (vote_title_user_id)
);


-- ----------------------------
-- Table structure for aoa_vote_titles
-- ----------------------------
DROP TABLE IF EXISTS aoa_vote_titles;
CREATE TABLE aoa_vote_titles (
  title_id serial,
  color varchar(255) DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  vote_id int DEFAULT NULL,
  PRIMARY KEY (title_id)
);